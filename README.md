# Advent of Clojure 2019

A Clojure library designed to solve advent of code 2019.

## Usage

Currently, you have to open the project in IntelliJ/Cursive, change project.clj to 
reflect the day, specifically :init-ns clojureX.core
where X is 1 2 ... 9 10 11 ... 24.

## License

You can copy-paste as you please.

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
