(ns clojure3.core)
(require '[clojure.string :as s])

;Day 3
(def inputs (s/split-lines (slurp "resources/input03.txt")))
(def line1 (s/split (first inputs) #","))
(def line2 (s/split (second inputs) #","))
;(def line1 (s/split "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51" #","))
;(def line2 (s/split "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7" #","))
(def maxdist 10000) ; Part 2, guessed 10000 "your answer is too high"
(defn mandist [c]
  (reduce + (map #(Math/abs %) c)))
(defn index-of [e coll] (first (keep-indexed #(if (= e %2) %1) coll)))

(defn run-prog []
  (def coords (atom []))
  (def intersects (atom []))
  (def intersteps (atom []))
  (def pos (atom [0 0])) ;x ->,  y ^
  (def steps (atom 0))
  (doseq [el line1]
    (let [dir (first el) len (read-string (apply str (rest el)))]
        (dotimes [i len]
          (case dir
            \D (reset! pos [(first @pos) (dec (second @pos))]) ;could/should probably do this with swap...
            \U (reset! pos [(first @pos) (inc (second @pos))])
            \R (reset! pos [(inc (first @pos)) (second @pos)])
            \L (reset! pos [(dec (first @pos)) (second @pos)]))
          (swap! steps inc)
          (if (< @steps maxdist)     ; don't append to the vector if more than 10000 steps, could also check mandist for part 1 only.
            (swap! coords conj @pos))
          )))
  (reset! pos [0 0])
  (reset! steps 0)
  (doseq [el line2]
    (let [dir (first el) len (read-string (apply str (rest el)))]
        (dotimes [i len]
          (case dir
            \D (reset! pos [(first @pos) (dec (second @pos))])
            \U (reset! pos [(first @pos) (inc (second @pos))])
            \R (reset! pos [(inc (first @pos)) (second @pos)])
            \L (reset! pos [(dec (first @pos)) (second @pos)]))
          (swap! steps inc)
          (if (some #(= @pos %) @coords)
            (do (swap! intersteps conj (+ 1 @steps (index-of @pos @coords))) ;and one for the Javanisse
              (swap! intersects conj @pos))))))
  (println "part 1:" (apply min (map mandist @intersects)))
  (println "part 2:" (apply min @intersteps)))


(run-prog)



