(ns clojure5.intcode)
(require '[clojure.string :as s])

;Day 5..
(def padding 3) ; integer telling how many 0 to pad/add at end.

(defn run-prog
  ([prog inp retfirst]
    (def prog-array (long-array (concat prog (repeat padding 0 )))) ;make a java array, with zeropadding

    (defn- get-val [idx] ;does not throw exception but return 0 if out of bounds.
      (if (< idx (alength prog-array))
        (aget prog-array idx)
        nil))
    
    (loop [idx 0 inplist inp]
      (let [[op p1 p2 p3] (map get-val (range idx (+ idx 4)))] ;fetch opcode and params at index
        (let [[A B C mop] [(< 10000 op) (< 1000 (mod op 10000)) (< 100 (mod op 1000)) (mod op 100)]]
          (let [v1 (if C p1 (get-val p1)) ;if A B C false: position mode, if true: immediate mode
                v2 (if B p2 (get-val p2))
                v3 (if A p3 (get-val p3)) ]
            (case mop       ;operations affecting memory
              1 (aset prog-array p3 (+ v1 v2))  ;add values
              2 (aset prog-array p3 (* v1 v2))  ;mul values
              3 (aset prog-array p1 (first inplist)) ;input "hardcoded" values
              4 (if (not retfirst) (println "OUTPUT:" v1))          ;output value
              7 (aset prog-array p3 (if (< v1 v2) 1 0))
              8 (aset prog-array p3 (if (= v1 v2) 1 0))
              nil);(println "Unknown opcode" op))
            (case mop          ;operations affecting instruction pointer/recursion
              99 (println "EXIT" (aget prog-array 0)) ;return and print 0th register (legacy)
              3 (recur (+ idx 2) (rest inplist))
              5 (recur (if (not= 0 v1) v2 (+ idx 3)) inplist) ;jump instrs
              6 (recur (if (= 0 v1) v2 (+ idx 3)) inplist)
              4 (if (true? retfirst) v1 (recur (+ idx 2) inplist))
              (recur (+ idx (cond ;how much to skip for non-jumps
                              (some #{mop} '(1 2 7 8)) 4
                              (some #{mop} '(3 4)) 2)) inplist)))))))
  ([prog inp]
   (run-prog prog inp false)))




