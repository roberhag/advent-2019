(ns clojure5.core)
(require '[clojure.string :as s])
(require 'clojure5.intcode)

;Day 5..
(def input (map read-string (s/split (slurp "resources/input05.txt") #",")))
;(def input (map read-string (s/split "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,
;0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99" #","))) ;test

;Day 5 part 1
(clojure5.intcode/run-prog input [1])

;Day 5 part 2
(clojure5.intcode/run-prog input [5])




