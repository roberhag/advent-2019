(ns clojure6.core)
(require '[clojure.string :as s])
(require '[clojure.set :as st])

;Day 6..
(defn readlines
  "Reads each line and returns a seq"
  [fname]
  (s/split-lines (slurp fname)))

(def input (readlines "resources/input06.txt"))
;(def input (s/split-lines "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN"))
(def pairs (for [line input]
             [(first (s/split line #"\)" )) (last (s/split line #"\)"))]))

(def kidmap (hash-map))
(def parmap (hash-map))
(doseq [p pairs] ;not very proud of def and assoc within doseq...
  (def kidmap (assoc kidmap (keyword (first p)) (cons (second p) ((keyword (first p)) kidmap []))))
  (def parmap (assoc parmap (keyword (second p)) (first p))))

;Day 6 part 1
(defn count-orbs [starting]
  (loop [children starting
         depth 0
         counter 0]
    (if (empty? children)
      counter
      (recur (apply concat (for [x children] ((keyword x) kidmap []))) (inc depth) (+ counter (* depth (count children)))))))

(println (count-orbs ["COM"]))

;Day 6 part 2
(defn make-parents [starting]
  (loop [parent starting
         parlist []]
    (if (= parent "COM")
      parlist
      (recur ((keyword parent) parmap) (conj parlist parent)))))

(def youpar (set (rest (make-parents "YOU"))))
(def sanpar (set (rest (make-parents "SAN"))))
(println (count (st/difference (st/union youpar sanpar) (st/intersection youpar sanpar))))