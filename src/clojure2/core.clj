(ns clojure2.core)
(require '[clojure.string :as s])

;Day 2.. I don't think Clojure is made for this
(def input (map read-string (s/split (slurp "resources/input02.txt") #",")))
;(def input (map read-string (s/split "1,9,10,3,2,3,11,0,99,30,40,50" #","))) ;test
;(def input (map read-string (s/split "1,1,1,4,99,5,6,0,99" #","))) ;test

(defn run-prog [noun verb]
  (def prog-array (long-array input)) ;Actually, it sort of works when using Java arrays.
  (defn- get-val [idx]
    (aget prog-array idx))
  (aset prog-array 1 noun)
  (aset prog-array 2 verb)
  (loop [idx 0]
    (let [op (get-val idx)] ;fetch opcode at index
      (if (= op 99) ;hopefully the only special case
        (aget prog-array 0) ;return and print
        (let [[a1 a2 tgt] (map get-val (range (+ idx 1) (+ idx 4)))] ;fetch the following parameters (arg1 arg2 target)
          (aset prog-array tgt
                (case op
                  1 (+ (get-val a1) (get-val a2))  ;add values at position a1 and position a2
                  2 (* (get-val a1) (get-val a2))  ;mul values at position a1 and position a2
                  (println "Unknown opcode" op)))
          (recur (+ idx 4)))))))

;Day 2 part 1
(println "Part 1:" (run-prog 12 2))

;Day 2 part 2
(println "part 2:" (for [n (range 100) v (range 100)
                         :let [result (run-prog n v)]
                         :when (= result 19690720)]
                     (+ (* 100 n) v)))


