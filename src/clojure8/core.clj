(ns clojure8.core)
(require '[clojure.string :as s])
(require '[clojure.set :as st])

;Day 8.
(def digits (slurp "resources/input08.txt"))
(def layer-size (* 6 25))
(def layers (for [x (range 0 (dec (count digits)) layer-size)] (subs digits x (+ x layer-size))))

;part 1
(def zeros (map-indexed vector (for [d layers] (count (for [c d :when (= c \0)] 1)))))
(def index-least (first (first (filter #(= (apply min (for [z zeros] (second z))) (second %)) zeros))))
(println (let [d (nth layers index-least)] (* (count (for [c d :when (= c \1)] 1)) (count (for [c d :when (= c \2)] 1)))))

;part 2


