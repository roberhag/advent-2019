(ns clojure7.core)
(require '[clojure.string :as s])
(require 'clojure5.intcode)

;Day 7
(def program (map read-string (s/split (slurp "resources/input07.txt") #",")))
;(def program (map read-string (s/split "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0" #","))) ;test

;(def padding 3) ; integer telling how many 0 to pad/add at end.

;Day 7 part 1
(defn chain-prog [prog phases]
  (loop [phi 0 inp 0] ;phase index, first input
    (if (< phi (count phases))
      (recur (inc phi) (clojure5.intcode/run-prog prog [(nth phases phi) inp] true))
      inp)))

;(println (apply concat (map vector [\a \b \c \d \e] (repeat 5 (range 3)))))
;I ended up with this, but it is not very idiomatic!
;(println (for [a (range 5) b (range 5) c (range 5) d (range 5) e (range 5) :when (apply distinct? [a b c d e])]  [a b c d e]))

;Part 1
(println (apply max (for [a (range 5) b (range 5) c (range 5) d (range 5) e (range 5) :when (apply distinct? [a b c d e])] (chain-prog program [a b c d e]) )))

;Part 2 wow not right now. Will try threads with some shared memory and locks, but I have to re-write my VM.




