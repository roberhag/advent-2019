(ns clojure4.core)

;day 4
(def minval 234208) ;hardcoded input
(def maxval 765869)

(defn valid-pass1 [p] ;p assumed string of digits
  (and (apply <= (for [c p] (int c))) ;monotonically <=
       (some true? (map #(apply = %) (partition 2 1 p))))) ;at least one pair of consecutive equal digits

(defn valid-pass2 [p] ;p assumed string of digits
  (def trufal (map #(apply = %) (partition 2 1 p))) ; sequence of 5 booleans, telling if two and two digits are equal.
  (and (valid-pass1 p)
       (or (and (first trufal) (not (second trufal)))                              ;first two but not first three same
           (and (last trufal) (not (last (butlast trufal))))                       ;last two but not last three same
           (some true? (map #(= '(false true false) %) (partition 3 1 trufal)))))) ;any pair surrounded by other digits


;part 1
(println (count (for [v (range minval (inc maxval)) :when (valid-pass1 (str v))] 1)))
;part 2
(println (count (for [v (range minval (inc maxval)) :when (valid-pass2 (str v))] 1)))

