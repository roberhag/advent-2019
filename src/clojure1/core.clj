(ns clojure1.core)
(require '[clojure.string :as s])

(defn readlines
  "Reads each line and returns a seq"
  [fname]
  (s/split-lines (slurp fname)))

;convert each string to int
(def input (map read-string (readlines "resources/input01.txt")))

;Day 1 part 1, seems ok
(defn div3-sub2 [x]
  (- (int (Math/floor (/ x 3))) 2))

(println (reduce + (map div3-sub2 input)))

;Okay, part 2 requires BLACK RECURSION MAGIC
(defn rocket-recur [base]
  (loop [acc 0    ;accumulator
         chunk base] ;chunk to calculate fuel for
    (let [step (div3-sub2 chunk)]
      (if (>= 0 step) ;if no more fuel needed
        acc
        (recur (+ acc step) step))))) ;recur on the extra fuel added in step

(println (reduce + (map rocket-recur input)))