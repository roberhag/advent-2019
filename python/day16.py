import numpy as np
from numba import jit, njit

with open("input16.txt", "r") as infile:
    instr = infile.read()

#instr = "80871224585914546619083218645595"
inp = []
for c in instr.strip():
    inp.append(int(c))

@njit
def fft(finp, n):
    base_pattern = np.asarray([0, 1, 0, -1], dtype=np.int32)
    l = len(finp)
    temp = np.zeros(l, dtype=np.int32)
    for k in range(n):
        print("phase", k+1)
        temp[:] = 0
        for i in range(l):
            for j in range(i, l):
                idx = (j+1)//(i+1)  #maybe it's possible to skip past many j because there are i+1 zeroes.
                if idx % 2:
                    patt = base_pattern[idx % 4]
                    temp[i] += (finp[j]*patt)
            temp[i] = (abs(temp[i]) % 10)
        finp[:] = temp[:]
    return finp


@njit
def fft2(finp, n):
    print("k")
    base_pattern = np.asarray([0, 1, 0, -1], dtype=np.int32)
    l = len(finp)
    temp = np.zeros(l, dtype=np.int32)
    for k in range(n):
        print("phase", k+1)
        temp[:] = 0
        for i in range(l):
            j = i
            while j < l:
                idx = (j+1)//(i+1)
                if idx % 2:
                    patt = base_pattern[idx % 4]
                    temp[i] += (finp[j]*patt)
                    j += 1
                else:
                    j += (i+1)
            temp[i] = (abs(temp[i]) % 10)
        finp[:] = temp[:]
    return finp

# Day 16 part 1
sinp = np.asarray(inp, dtype=np.int32)
print("".join(str(x) for x in fft2(sinp, 100)[:8]))

# Day 16 part 2
linp = np.asarray(inp*10000, dtype=np.int32)
print("".join(str(x) for x in fft2(linp, 100)[:8]))
