from intcodes import Intcodes
import numpy as np

with open("input17.txt") as inf:
    instr = inf.read()

# Day 17 p 1
game = Intcodes(instr, ascii=True)
outie = game.run_prog()

outie2 = """..#..........
..#..........
##O####...###
#.#...#...#.#
##O###O###O##
..#...#...#..
..#####...^.."""

def make_intersect(board):
    aligsum = 0
    lines = board.split("\n")
    ht = len(lines)
    wt = len(lines[0].strip())
    board = np.zeros([wt, ht], dtype=np.uint8)
    for y, line in enumerate(lines):
        for x, c in enumerate(line.strip()):
            if c in "#<>v^O":
                board[x,y] = 1
    for x in range(1, wt-1):
        for y in range(1, ht-1):
            if board[x,y] and board[x,y+1] and board[x+1,y] and board[x,y-1] and board[x-1,y]:
                aligsum += x*y
    return aligsum

# Day 17 part 1
print(make_intersect(outie))

# Day 17 part 2 ... wow, Luckily I found this by eye rather easily:
inputs = """A,B,A,B,C,C,B,A,B,C
L,12,L,10,R,8,L,12
R,8,R,10,R,12
L,10,R,12,R,8
y

"""
inputs = [ord(c) for c in inputs]
# ascii code for newline is ord("\n") = 10
game = Intcodes(instr, ascii=True, retfirst=False)
game.program[0] = 2
outie = game.run_prog(inputs)