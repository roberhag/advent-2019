import numpy as np

with open("input20.txt", "r") as infile:
    instr = infile.read()

instr2 = """                   A               
                   A               
  #################.#############  
  #.#...#...................#.#.#  
  #.#.#.###.###.###.#########.#.#  
  #.#.#.......#...#.....#.#.#...#  
  #.#########.###.#####.#.#.###.#  
  #.............#.#.....#.......#  
  ###.###########.###.#####.#.#.#  
  #.....#        A   C    #.#.#.#  
  #######        S   P    #####.#  
  #.#...#                 #......VT
  #.#.#.#                 #.#####  
  #...#.#               YN....#.#  
  #.###.#                 #####.#  
DI....#.#                 #.....#  
  #####.#                 #.###.#  
ZZ......#               QG....#..AS
  ###.###                 #######  
JO..#.#.#                 #.....#  
  #.#.#.#                 ###.#.#  
  #...#..DI             BU....#..LF
  #####.#                 #.#####  
YN......#               VT..#....QG
  #.###.#                 #.###.#  
  #.#...#                 #.....#  
  ###.###    J L     J    #.#.###  
  #.....#    O F     P    #.#...#  
  #.###.#####.#.#####.#####.###.#  
  #...#.#.#...#.....#.....#.#...#  
  #.#####.###.###.#.#.#########.#  
  #...#.#.....#...#.#.#.#.....#.#  
  #.###.#####.###.###.#.#.#######  
  #.#.........#...#.............#  
  #########.###.###.#############  
           B   J   C               
           U   P   P               """

instr1 = """         A           
         A           
  #######.#########  
  #######.........#  
  #######.#######.#  
  #######.#######.#  
  #######.#######.#  
  #####  B    ###.#  
BC...##  C    ###.#  
  ##.##       ###.#  
  ##...DE  F  ###.#  
  #####    G  ###.#  
  #########.#####.#  
DE..#######...###.#  
  #.#########.###.#  
FG..#########.....#  
  ###########.#####  
             Z       
             Z       """


lines = instr.split("\n")
ht = len(lines)
wt = len(lines[0])
if len(lines[-1]) == 0:
    ht -= 1  # the last line is empty

board = np.zeros([wt+2, ht+2], dtype=np.int32) -1  # -1 are walls.
# We have an extra buffer around. both x and y coords count one extra
neighs = (-1,0),(1,0),(0,-1),(0,1)
# Give all portals negative numbers, just the letter-values ord(letter), times 100 for first digit
# AA = -(65*100 + 65)
#
# ZZ = -9090 etc... we start the BFS at ZZ,
ZZ = -9090
AA = -6565


for x in range(0, wt-1):
    for y in range(0, ht-1):
        if lines[y][x] == ".":
            board[x+1,y+1] = 1000000
        oc = ord(lines[y][x])
        if 65 <= oc <= 90:
            # We have a letter A-Z
            oc2 = ord(lines[y+1][x])
            oc3 = ord(lines[y][x+1])
            if 65 <= oc2 <= 90:
                n = 100*oc + oc2
                board[x+1,y+1] = -n
                board[x+1,y+2] = -n
            elif 65 <= oc3 <= 90:
                n = 100*oc + oc3
                board[x+1,y+1] = -n
                board[x+2,y+1] = -n

# for line in board.T:
#     for c in line:
#         if c < -1:
#             print(chr(-c//100), end="")
#         elif c == -1:
#             print("#", end="")
#         else:
#             print(" ", end="")
#     print()

def next_steps(id, n):
    next_list = []
    for x,y in zip(*np.where(board==id)):
        for w,z in ((x+i, y+j) for i,j in neighs):
            if board[w,z] > n:
                next_list.append((w,z))
            if board[w,z] != id and board[w,z] < -1:  # We hit a portal.
                if board[w,z] == AA:  # We hit finish
                    return None
                next_list = next_list + next_steps(board[w,z], n)
    return next_list

next_list = next_steps(ZZ, 0)
step = 1
while next_list:
    for x, y in next_list:
        board[x,y] = step
    next_list = next_steps(step, step)
    step += 1

for line in board.T:
    for c in line:
        if c < -1:
            print(chr(-c//100), end="")
        elif c == -1:
            print("#", end="")
        elif c > 100000:
            print(" ", end="")
        else:
            print(c % 10, end="")
    print()

# day2, part 1
print(step - 2)  # do not count the final tile or the start.

# Day 20 part 2
# Idea: after reading the maze/board, make N copies of it for each layer.
# Somehow identify if a portal goes inward or outward, swap the layer of the maze with each pass.
