import numpy as np
import operator as op
from collections import defaultdict
from pynput.keyboard import Key, Listener
from os import system

lr_inp = 0

def on_press(key):
    global lr_inp
    print("Key pressed", str(key))
    if key == Key.left:
        lr_inp = -1
    if key == Key.right:
        lr_inp = 1

def on_release(key):
    global lr_inp
    lr_inp = 0
    if key == Key.esc:
        # Stop listener
        return False

# Collect events until released
listener = Listener(
        on_press=on_press,
        on_release=on_release)

# listener.join()


code_dict = {  # dictionary of all operators that don't jump
    1: op.add,
    2: op.mul,
    7: op.lt,
    8: op.eq,
}

jump_dict = {
    5: lambda x: x != 0,
    6: lambda x: x == 0
}

incr_dict = {
    1: 4,
    2: 4,
    3: 2,
    4: 2,
    5: 3,  # still increase 3 if no jump
    6: 3,
    7: 4,
    8: 4,
    9: 2
}

class Intcodes:

    def make_prog(self, prog_string):
        program = [int(x) for x in prog_string.split(",")]
        prog_dict = defaultdict(int)
        for i in range(len(program)):
            prog_dict[i] = program[i] 
        return prog_dict  # np.asarray(program, dtype=np.int64)


    def run_prog(self, inputs=[]):
        global lr_inp
        if isinstance(inputs, (int, np.int64)):
            inputs = [inputs]
        if inputs is None:
            return None
        if self.retfirst and self.ascii:
            retstr = ""
        chv = ""
        while True:
            writeit = None
            op, p1, p2, p3 = (self.program[self.idx + i] for i in range(4))
            A, B, C, mop = ((op // 10000), ((op % 10000) // 1000), ((op % 1000) // 100), (op % 100)) 
            # print(f"{op} ({mop}, {(A,B,C)}. Params = {(p1, p2, p3)}")
            if C == 1:  #  Mode of parameter 1 ...
                v1 = p1
            elif C == 2:
                v1 = self.program[self.rb + p1]
            else:
                v1 = self.program[p1]
            if B == 1:
                v2 = p2
            elif B == 2:
                v2 = self.program[self.rb + p2]
            else:
                v2 = self.program[p2]
            if A == 1:
                v3 = p3
            elif A == 2:
                v3 = self.program[self.rb + p3]
            else:
                v3 = self.program[p3]
            if mop in code_dict:  # standard operators
                if A == 2:
                    self.program[self.rb + p3] = code_dict[mop](v1, v2)
                else:
                    self.program[p3] = code_dict[mop](v1, v2)
            if mop == 3:  # INPUT
                if self.gk:  # get key as input
                    inputs = [lr_inp]
                if len(inputs) == 0:
                    # print("No more inputs?!")
                    return None  # return without increasing idx counter. Should continue at input next call.
                if C == 2:
                    self.program[self.rb + p1] = inputs.pop(0)
                else:
                    self.program[p1] = inputs.pop(0)
            if mop == 4:  # OUTPUT
                #print(f"output {v1}")
                if self.ascii:  # TODO, save previous output for newlinenewline, otherwie always print chr
                    if v1 > 128:  # non-ASCII
                        print(v1)
                        chv = ""
                    else:
                        if (not self.retfirst) and (chr(v1) == "\n") and (chv == "\n"):
                            system("clear")  # Two newlines clears the screen
                        chv = chr(v1)
                        if self.retfirst:
                            retstr = retstr + chv
                        else:
                            print(chv, end="")
                elif self.retfirst:
                    self.idx += 2
                    return v1
                else:
                    print(v1)
            if mop == 9:
                self.rb += v1
            if mop == 99:
                # print("arrived at 99")
                if self.ascii and self.retfirst:
                    return retstr
                return None
            if (mop in jump_dict) and (jump_dict[mop](v1)):
                self.idx = v2
            else:
                self.idx += incr_dict[mop]


    def __init__(self, fname, retfirst=True, getkey=False, ascii=False):
        self.program = self.make_prog(fname)
        self.idx = 0
        self.rb = 0  # relative base
        self.gk = getkey
        self.ascii = ascii  # interpret output numbers as ascii through chr(value)
        self.retfirst = retfirst  # retfirst means return first output. if ascii, all output is accumulated.
