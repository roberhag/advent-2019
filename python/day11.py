from intcodes import Intcodes
from collections import defaultdict, deque
import matplotlib.pyplot as plt
# Defining a right hand cartesian coordinate system, where x is right and y is up/north

with open("input11.txt") as inf:
    instr = inf.read()

def paintit(initial = 0):
    colours = defaultdict(int)  # zero = black is standard
    dr = deque(((0, 1), (1, 0), (0, -1), (-1, 0)))  # the directions, from starting up, and to the right.
    pos = (0,0)
    bot = Intcodes(instr, True)
    colours[pos] = initial
    # Day 11 p 1
    while True:
        colour = bot.run_prog(colours[pos])
        turn = bot.run_prog()  # Can give it infinitely many of this?
        if (colour is None) or (turn is None):
            break
        colours[pos] = colour
        if turn:  # Rotate right
            dr.rotate(-1)
        else:
            dr.rotate(1)  # Rotate left
        pos = (pos[0] + dr[0][0], pos[1] + dr[0][1])
    return colours

# Day 11 part 1
colours1 = paintit(0)
print(len(colours1))

# Day 11 part 2
colours = paintit(1)
w_x = []
w_y = []
for c in colours:
    if colours[c] == 1:
        w_x.append(c[0])
        w_y.append(c[1])
plt.scatter(w_x, w_y)
plt.show()
