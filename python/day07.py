from intcodes import Intcodes
import itertools

lc = 5  # Length of chain
with open("input07.txt") as inf:
    instr = inf.read()

# Day 7 p 1
phase_perm = itertools.permutations(range(5))
max_sig = 0
for phases in phase_perm:
    sig = 0
    chain = [Intcodes(instr) for i in range(lc)]  # reset chain
    for i in range(lc):
        sig = chain[i].run_prog([phases[i], sig])  # Phase setting first input, current sig second
    max_sig = max(sig, max_sig)
print(max_sig)

# Day 7 p 2

phase_perm = itertools.permutations(range(5, 10))
max_sig = 0
for phases in phase_perm:
    sig = 0
    chain = [Intcodes(instr) for i in range(lc)]  # reset chain
    for i in range(lc):  # Go through whole chain once to set inputs.
        nsig = chain[i].run_prog(phases[i])
    while True:
        nsig = sig
        for i in range(lc):  # send through whole chain once.
            nsig = chain[i].run_prog(nsig)  # current sig
        if nsig is None:  # reached program halt, intcode 99
            break
        sig = nsig
    # print(f"Phases {phases} gives signal {sig}.")
    max_sig = max(sig, max_sig)
print(max_sig)