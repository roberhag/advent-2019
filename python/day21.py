from intcodes import Intcodes
import numpy as np

with open("input21.txt") as inf:
    instr = inf.read()

game = Intcodes(instr, ascii=True)

# Day 21 part 1

# If there is any kind of hole, and the fourth is ground, jump
inputs = """NOT A J
NOT B T
OR T J
NOT C T
OR T J
AND D J
WALK
"""
inputs = [ord(c) for c in inputs]
game = Intcodes(instr, ascii=True, retfirst=False)
game.run_prog(inputs)

# Day 21 part 2

# If there is any kind of hole, and the fourth is ground, jump

# ... except we should NOT jump if that puts us on an island from which we cannot jump, eg
# ABCDEFGHI
#@T??TF??F
"""
NOT H T
AND A T  # Temp is true if H is hole and A is ground
NOT T T  # False if ""
AND T J  # If we wanted to jump AND the above not happening
NOT D T 
NOT T T
AND T J"""


inputs = """NOT A J
NOT B T
OR T J
NOT C T
OR T J
AND D J
NOT H T
AND A T
NOT T T
AND T J
RUN
"""
inputs = [ord(c) for c in inputs]
game = Intcodes(instr, ascii=True, retfirst=False)
game.run_prog(inputs)