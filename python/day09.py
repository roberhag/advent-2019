from intcodes import Intcodes
import itertools

lc = 5  # Length of chain
with open("input09.txt") as inf:
    instr = inf.read()

# Day 9 p 1
sig = Intcodes(instr, False).run_prog(1)
# Day 9 p 2
sig = Intcodes(instr, False).run_prog(2)