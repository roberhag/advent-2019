from collections import defaultdict

with open("input14.txt", "r") as infile:
    instr = infile.read()

lines = instr.strip().split("\n")
reac_dict = {}

for line in lines:
    reagents, product = line.split(" => ")
    reagents = reagents.split(", ")
    np, product = product.strip().split(" ")
    lhs = [int(np)]  # first element is how many of the product that is made
    for r in reagents:
        nr, r = r.strip().split(" ")
        lhs.append([int(nr), r])
    if product in reac_dict:
        print("Duplicate not implemented!", product)
    reac_dict[product] = lhs

# TODO: I think this can be done effectively recursively in a functional language.

def make(tgt, n_tgt):
    have = defaultdict(int)
    want = defaultdict(int)
    want[tgt] = n_tgt
    ore = 0
    len_w = 1
    while len_w > 0:
        amnt = want[tgt]
        want[tgt] = 0  # remove from most wanted
        m_n_tgt = reac_dict[tgt][0]
        if amnt % m_n_tgt == 0:  # exact
            multi = amnt // m_n_tgt 
            lefto = 0
        else:
            multi = (amnt // m_n_tgt) + 1  # need one extra reaction to make enough
            lefto = (multi*m_n_tgt) - amnt
            # print(f"Gonna make total {(multi*m_n_tgt)}. Gonna have {lefto} leftovers.")
        to_make = reac_dict[tgt][1:]
        # print(f"Making {tgt}, multi {multi}")
        for m in to_make:
            m_0 = m[0]*multi  # how many we need of this
            if m[1] == "ORE":
                ore += m_0
                # print(f"added {m_0} ore")
                continue
            have_m1 = have[m[1]]
            # print(f"okay, we want {m_0} of {m[1]}, and we have {have_m1}.")
            if m_0 < have_m1:  # we actually have more than enough
                have[m[1]] -= m_0
                # print(f"we have enough of {m[1]}")
                continue
            else:
                have[m[1]] = 0
                m_0 = m_0 - have_m1
            want[m[1]] += m_0
        have[tgt] += lefto
        # Counting, cleanup and choose new target
        big_num = 0
        big_k = None
        for k in list(want.keys()):
            if want[k] == 0:
                want.pop(k)  # remove empty keys
                continue

            if want[k] > big_num:
                big_k = k
                big_num = want[k]
        tgt = big_k
        len_w = len(want)
        #print("want:", want)
        #print("ore:", ore, "... have", dict(have))
        #print() 

    #print("Leftovers:", dict(have))
    return ore

# Part 1
a = make("FUEL", 1)
print(a)
# Part 2
n_ore = 1000000000000
n_fuel = n_ore//a  # First guess.
dn = n_fuel//100
while True:
    a = make("FUEL", n_fuel)
    if a > n_ore:
        if dn > 1:
            n_fuel -= dn
            dn = max(dn//10, 1)
        else:
            n_fuel -= 1
            break
    n_fuel += dn
print(n_fuel)