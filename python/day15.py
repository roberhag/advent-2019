from intcodes import Intcodes
import matplotlib.pyplot as plt
#import numpy as np
# TODO ... rest of the owl.
# Idea: move forward until wall, turn left, follow wall with wall on right hand side.
# problem: not failsafe, what if we hit an island and just follow it around?
# Idea: make interactive program that paints/displays the map realtime?

with open("input15.txt") as inf:
    instr = inf.read()

def make_board():
    blocks = set()
    x, y = 0, 0
    ox = 0
    game = Intcodes(instr)
    inp = 0
    while True:
        ret = game.run_prog(di)
        if ret == 1:
            walls.add((x, y))
        if ret == 2:
            ox = (x, y)
    return walls, ox

# Day 15 p 1
walls, oxygen = make_board()
print(len(blocks))
