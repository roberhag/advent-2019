import numpy as np


def simulate(steps = 1000, cycle = False):
    # First index is for moon, second index is for xyz
    xs = np.zeros([4,3], dtype = np.int64)
    vs = np.zeros([4,3], dtype = np.int64)

    with open("input12.txt") as inf:
        for i in range(4):
            derp = list(x.split("=")[1] for x in inf.readline().replace(">", "").split(", "))
            xs[i, :] = derp
    initial = xs.copy()
    cycles = np.zeros(3, dtype=np.int64)

    for i in range(steps):
        if i % 1000000 == 0:
            print("step:", i)
        for m1 in range(3):  # don't need to go through the last, because it'll get automagic grav
            for m2 in range(m1+1, 4):  # go through all the next ones
                dx = xs[m2] - xs[m1]
                g = 1*(dx > 0) - 1*(dx < 0)  # masks out to +1 if positive and -1 if negative. 0 if 0
                vs[m1] += g
                vs[m2] -= g  # every action has an equal and opposite reaction
        xs += vs  # yup, this is the step for updating all positions.
        if cycle:  # Looking for a great cycle
            equal = np.sum((xs == initial), axis=0)
            if (equal == 4).any():
                idxs = np.where(equal == 4)[0]
                for idx in idxs:
                    if cycles[idx] == 0:
                        cycles[idx] = i + 2
                if (cycles != 0).all():
                    # Here we have filled all cycles
                    return cycles
    return xs, vs  # The final pos and vel

def etot(xs, vs):
    return np.sum(np.sum(np.absolute(xs), axis=1)*np.sum(np.absolute(vs), axis=1))

xs, vs = simulate(1000)
print(etot(xs, vs))

cyc = simulate(1000000, True)
print(cyc)
print(np.lcm.reduce(cyc))
