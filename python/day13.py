from intcodes import Intcodes
import matplotlib.pyplot as plt
import numpy as np
plt.ion()  # All lines of commented code are for visualisation.

with open("input13.txt") as inf:
    instr = inf.read()

def make_board():
    blocks = set()
    game = Intcodes(instr)
    while True:
        x = game.run_prog()
        y = game.run_prog()
        tile = game.run_prog()
        if None in (x, y, tile):
            break
        if tile == 2:
            blocks.add((x, y))
    return blocks

# Day 13 p 1
blocks = make_board()
print(len(blocks))

def move(bx, px):  # Ball x, paddle x
    if px < bx:  # ball to the right of paddle
        return 1
    elif px > bx:  # ball to the left
        return -1
    else:
        return 0

def play_game():
    game = Intcodes(instr)
    ball = None
    paddle = None
    game.program[0] = 2  # insert two quarters
    board = np.zeros([50, 40])
    # pltobj = plt.imshow(board.T, vmin=0, vmax = 1)
    i = 0
    inp = 0
    score = 0
    while True:
        i += 1
        x = game.run_prog(inp)
        y = game.run_prog(inp)
        tile = game.run_prog(inp)
        if None in (x, y, tile):
            break
        if (x == -1) and (y == 0):
            #print("Score:", tile)
            score = tile
        else:
            board[x, y] = tile/4
        if tile == 3:  # Paddle
            if ball:
                inp = move(ball[0], x)
            paddle = x
        if tile == 4:
            if paddle:
                inp = move(x, paddle)
            ball = (x, y) 
            # pltobj.set_data(board.T)
            # plt.draw()
            # plt.pause(1e-17)
    print("Score:", score)

# Day 13 p 2
play_game()