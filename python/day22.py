import numpy as np

print(f"uint16 max = {2**15}, uint32 max = {2**31}, uint64 max = {2**63}.")

with open("input22.txt", "r") as infile:
    instr = infile.read()

def shuffle(n, instr, repeat=1, pos=None):
    deck = np.arange(n, dtype=np.uint64)
    temp = np.arange(n, dtype=np.uint64)
    for line in instr.split("\n"):
        if "new stack" in line:
            deck[:] = np.flip(deck)
        elif "cut" in line:
            idx = int(line[4:])
            temp[-idx:] = deck[:idx]
            temp[:-idx] = deck[idx:]
            deck[:] = temp[:]
        elif "increment" in line:
            inc = int(line.split("increment ")[1])
            #print(line)
            j = 0
            i = 0
            while i < n:
                nd = ((n - j)//inc)
                if ((n - j) % inc):
                    nd += 1
                #print(i, j, nd)
                temp[j::inc] = deck[i:i + nd]
                #print(temp)
                j += inc*nd
                j %= n
                i += nd
            deck[:] = temp[:]

    if pos:
        return np.where(deck==pos)
    else:
        return deck

# day 22 part i
print(shuffle(10007, instr, repeat=1, pos=2019))

# day 22 part 2,
# okay, what the flop. The array of 119315717514047 uint64s is almost a petabyte memory!
# the actual way to solve this is to keep track of only position 2020, and trace the indices backwards.