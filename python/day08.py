import numpy as np

w = 25
h = 6
image = np.zeros([h, w], dtype=np.uint8) + 2  # initiate transparent. Saving memory with uint8 lol

with open("input08.txt", "r") as infi:
    strinp = infi.read().replace("\n", "")
layers = []
i = 0
while i < len(strinp):
    layers.append(np.asarray([int(x) for x in strinp[i : i + w*h]], dtype=np.uint8).reshape([h, w]))
    i += w*h

# part 2 only. Part 1 was done in clojure
for layer in layers:
    transp = (image == 2)
    image[transp] = layer[transp]

for li in image:
    print(str(li).replace("0", " ").replace("1", "#"))