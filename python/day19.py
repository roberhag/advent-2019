from intcodes import Intcodes
from copy import deepcopy
import numpy as np

with open("input19.txt") as inf:
    instr = inf.read()

def find_beam(instr, size, origin=(0,0)):
    game = Intcodes(instr)
    board = np.zeros([size, size], dtype=np.uint8)
    game.run_prog()  # do not give input, it should halt and be ready for input
    for y in range(size):
        for x in range(size):  # This shortcut works for my input.
            game_cpy = deepcopy(game)
            b = game_cpy.run_prog([x + origin[0], y + origin[1]])
            del game_cpy
            board[x,y] = b
    return board

# Day 19 part 1
print(np.sum(find_beam(instr, 50)))

#board = find_beam(instr, 150)
#np.save("board100.npy", board)
#board = np.load("board100.npy")
#for x in board.T:
#    print("".join([str(i) for i in x]).replace("0", " "))

# Okay, let's assume that we can extrapolate the pixelised lines.
# upper line, x 33  y 35, goes diagonally 34 steps until (67,69), 
# next diagonal line starts from (67,70)-(101,104), etc
def upper_x(y):
    return y - (y//35) - 1

# Lower line, (8,9) 7 steps to (15,16), (15,17) 7 steps (22,24), (22,25) etc
def lower_x(y):
    return y - ((y-1)//8)

y = 100
while ((upper_x(y) - lower_x(y)) < 100):
    y += 8
cord = (lower_x(y) + 50, y)

# board = find_beam(instr, 100, cord)  # We are at width 100, but not a square of 100x100

def move_square(origin=(0,0)):
    game = Intcodes(instr)
    game.run_prog()  # do not give input, it should halt and be ready for input
    x = origin[0]
    y = origin[1]
    while True:
        game_cpy = deepcopy(game)
        a = game_cpy.run_prog([x,y])
        del game_cpy
        if a == 0:  # We're far too right
            y -= 50
            continue
        game_cpy = deepcopy(game)
        b = game_cpy.run_prog([x, y + 99])
        del game_cpy
        game_cpy = deepcopy(game)
        c = game_cpy.run_prog([x + 99, y])
        del game_cpy
        if b + c == 2:
            return x, y
        if b + c == 0:  # both UR and LL corner are outside beam.
            x += 1
            y += 1
        elif b == 0:  # Lower left corner is outside, 
            x += 1
        else:
            y += 1

x, y = move_square(cord)
print(x, y)
print (x*10000 + y)