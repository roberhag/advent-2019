"""
My idea is: the relative coordinate between two asteroids can be divided by the least common divisor
(e.g. (4, 2) becomes (2, 1)), then all asteroids with a relative coordinate at some multiple of this are hidden.
"""
import numpy as np
from math import gcd  # Greatest common divisor

with open("input10.txt", "r") as inf:
    instri = inf.read()

instri2 = """.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##"""

asteroids = []
xmax, ymax = 0, 0
for y, l in enumerate(instri.split("\n")):
    ymax = max(ymax, y)
    for x, c in enumerate(l):
        xmax = max(xmax, x)
        if c ==  "#":
            asteroids.append((x, y))
xy_len = max(xmax, ymax)

min_hidden = len(asteroids)
best_coord = None
for a in asteroids:
    hidden = set()
    for b in asteroids:
        rel = (b[0] - a[0], b[1] - a[1])
        if rel == (0, 0):
            continue  # do not count yoursef.
        ngcd = gcd(rel[0], rel[1])
        small_rel = (rel[0]//ngcd, rel[1]//ngcd)
        small_len = max(abs(small_rel[0]), abs(small_rel[1]))
        n_replicas = xy_len//small_len 
        line = list((small_rel[0]*i, small_rel[1]*i) for i in range(1, n_replicas + 2))
        first = True
        for i, c in enumerate(line):  # straight line, going out from asteroid a.
            d = (a[0] + c[0], a[1] + c[1])
            if d in asteroids:
                if first:  # the first along the line is not hidden
                    first = False
                else:
                    hidden.add(d)
    n_hidden = len(hidden)
    if n_hidden < min_hidden:
        best_coord = a 
        min_hidden = n_hidden
print(len(asteroids) - min_hidden - 1)
print(best_coord)

# Part 2
# TODO: start at (31, 20), make list of all visible. Start directly (0, -1) and rotate in (1, -1) dir.
# After full rotation, make new list of all visible.

a = best_coord  # Should be (31, 20) for my input
vaporised = 0
asteroids = set(asteroids)  # Now we want to modify it
while vaporised < 200:
    visible = set()
    rel_visible = set()
    for b in asteroids:
        rel = (b[0] - a[0], b[1] - a[1])
        if rel == (0, 0):
            continue  # do not count yoursef.
        ngcd = gcd(rel[0], rel[1])
        small_rel = (rel[0]//ngcd, rel[1]//ngcd)
        small_len = max(abs(small_rel[0]), abs(small_rel[1]))
        n_replicas = xy_len//small_len 
        line = list((small_rel[0]*i, small_rel[1]*i) for i in range(1, n_replicas + 2))
        first = True
        for i, c in enumerate(line):  # straight line, going out from asteroid a.
            d = (a[0] + c[0], a[1] + c[1])
            if d in asteroids:
                if first:  # the first along the line is visible
                    rel_visible.add(c)  # add the relative coordinate for sorting
                    visible.add(d)
                    first = False
    if vaporised + len(visible) < 200:  # not destroyed 200 yet.
        vaporised += len(visible)
        print("vaporised ", vaporised)
        asteroids = asteroids.difference(visible)
    else:
        sorted_visible = sorted(list(rel_visible), key=lambda x: -np.arctan2(x[0], x[1]))
        a200 = sorted_visible[199 - vaporised]
        a200 = (a200[0] + a[0], a200[1] + a[1])
        print(a200[0]*100 + a200[1])
        vaporised = 200